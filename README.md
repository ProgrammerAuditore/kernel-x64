# Crear un kernel de x64

multiboot_header.asm
```shell
nasm multiboot_header.asm
nasm -f elf64 multiboot_header.asm
```
Como resultado genera un archivo binario llamado multiboot_header.o

boot.asm
```shell
nasm boot.asm
nasm -f elf64 boot.asm
```
Como resultado genera un archivo binario llamado boot.o


linker.ld
```shell
ld -n -o kernel.bin -T linker.ld multiboot_header.o boot.o
```
Como resultado genera un archivo llamado kernel.bin
Reemplazar _./isofiles/grub/kernel.bin_ por el archivo generado.

### Crear un imagen ISO
instalar _xorriso_ usando el siguiente comando:
```shell
sudo apt install xorriso
```

Para crear un imagen booteable se usa el siguiente comando:
```shell
grub-mkrescue -o os_64.bin isofiles
```

### Bootear el ISO con QEMU

Para botear el iso usando QEMU usa el siguiente comando:
```shell
qemu-system-i386 -curses -cdrom os_64.iso
```


